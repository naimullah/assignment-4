
function scoreCount() {
    var ques1 = document.forms["quizForm"]["ques1"].value;
    var ques2 = document.forms["quizForm"]["ques2"].value;
    var ques3 = document.forms["quizForm"]["ques3"].value;
    var ques4 = document.forms["quizForm"]["ques4"].value;
    var ques5 = document.forms["quizForm"]["ques5"].value;
    var ques6 = document.forms["quizForm"]["ques6"].value;
    var ques7 = document.forms["quizForm"]["ques7"].value;
    var ques8 = document.forms["quizForm"]["ques8"].value;
    var ques9 = document.forms["quizForm"]["ques9"].value;
    var ques10 = document.forms["quizForm"]["ques10"].value;
    var userName = document.forms["quizForm"]["uname"].value;
    var score = 0;
    if (ques1 == "") {
        alert("Oops!! Question 1 is required");
        return false;
    }
    else if (ques2 == "") {
        alert("Oops!! Question 2 is required");
        return false;
    }
    else if (ques3 == "") {
        alert("Oops!! Question 3 is required");
        return false;
    }
    else if (ques4 == "") {
        alert("Oops!! Question 4 is required");
        return false;
    }
    else if (ques5 == "") {
        alert("Oops!! Question 5 is required");
        return false;
    }
    else if (ques6 == "") {
        alert("Oops!! Question 6 is required");
        return false;
    }
    else if (ques7 == "") {
        alert("Oops!! Question 7 is required");
        return false;
    }
    else if (ques8 == "") {
        alert("Oops!! Question 8 is required");
        return false;
    }
    else if (ques9 == "") {
        alert("Oops!! Question 9 is required");
        return false;
    }
    else if (ques10 == "") {
        alert("Oops!! Question 10 is required");
        return false;
    }
    else if (userName == "") {
        alert("Name field is required");
        return false;
    }

    else {
        if (ques1 == "Cascading style sheets") {
            score += 1;
        }
        if (ques2 == "style") {
            score += 1;
        }
        if (ques3 == "background-color") {
            score += 1;
        }
        if (ques4 == "color") {
            score += 1;
        }
        if (ques5 == "font-size") {
            score += 1;
        }
        if (ques6 == "font-weight") {
            score += 1;
        }
        if (ques7 == "comma") {
            score += 1;
        }
        if (ques8 == "static") {
            score += 1;
        }
        if (ques9 == "demo") {
            score += 1;
        }
        if (ques10 == "yes") {
            score += 1;
        }

    }

    if (score > 4 && score < 10) {
        alert("Way to go, " + userName + "! You got " + score + " out of 10 correct");
    }
    else if (score == 10) {
        alert("Congratulations " + userName + "! You got " + score + " out of 10");
    }
    else { alert("Keep trying, " + userName + "! You answered " + score + " out of 10 correctly"); }


}